const elements = require('./data.cjs');

function pairs(elements) {
    const result = [];

    if (typeof(elements) !== 'object' || pairs.arguments.length !== 1) {
        return [];
    }

    for (let key in elements) {

        if (typeof(elements[key]) === 'function' || typeof(elements[key]) === 'object') {
            continue;
        }
        else {
            result.push([key, elements[key]]);
        }
    }
    return result;
}

// let a = pairs(elements);

// console.log(a);

module.exports = pairs;