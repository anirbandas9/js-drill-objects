const elements = require('./data.cjs');

function defaults(elements, defaultProps) {
    // const result = {};

    if (typeof(elements) !== 'object' || typeof(defaultProps) !== 'object' || defaults.arguments.length < 2) {
        return {};
    }

    for (let key in defaultProps) {
        if (elements[key] === undefined) {
            elements[key] = defaultProps[key];
        }
    }
    return elements;
}

// let a = defaults(elements, {flavor: "vanilla", sprinkles: "lots"});

// console.log(a);

module.exports = defaults;