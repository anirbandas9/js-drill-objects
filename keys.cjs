const elements = require('./data.cjs');

function keys(elements) {
    const result = [];

    if (typeof(elements) !== 'object' || keys.arguments.length !== 1) {
        return [];
    }
    
    for (let key in elements) {
        result.push(key);
    }
    return result;
}

// let a = keys(elements);

// console.log(a);

module.exports = keys;