const elements = require('./data.cjs');

function values(elements) {
    const result = [];

    if (typeof(elements) !== 'object' || values.arguments.length !== 1) {
        return [];
    }

    for (let key in elements) {
        // console.log(typeof(elements[key]));
        if (typeof(elements[key]) === 'function') {
            continue;
        }
        else {
            result.push(elements[key]);
        }
    }
    return result;
}

// let a = values(elements);

// console.log(a);

module.exports = values;

