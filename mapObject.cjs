const elements = require('./data.cjs');

function mapObject(elements, cb) {
    const resultObj = {};

    if (typeof(elements) !== 'object' || mapObject.arguments.length < 2) {
        return {};
    }
    
    for (let key in elements) {
        
        if (typeof(elements[key]) === 'function' || typeof(elements[key]) === 'object') {
            continue;
        }
        else {
            resultObj[key] = cb(elements[key]);
        }
    }
    return resultObj;
}

// let a = mapObject(elements, (val) => val * 2);

// console.log(a);

module.exports = mapObject;