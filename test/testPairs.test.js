const data = require('../data.cjs');
const pairs = require('../pairs.cjs');

test('Testing Pairs Function', () => {
    expect(pairs(data)).toStrictEqual(Object.entries(data));
});