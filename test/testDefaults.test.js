const elements = require("../data.cjs");
const defaults = require("../defaults.cjs");

const defaultProps = {flavor: "vanilla", sprinkles: "lots"};

const result = {
  name: "Bruce Wayne",
  age: 36,
  location: "Gotham",
  flavor: "vanilla",
  sprinkles: "lots",
};

test('Testing Defaults function', () => {
    expect(defaults(elements, defaultProps)).toStrictEqual(result);
});
