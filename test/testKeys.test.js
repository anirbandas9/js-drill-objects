const data = require('../data.cjs');
const keys = require('../keys.cjs');

test('Testing keys function', () => {
    expect(keys(data)).toStrictEqual(Object.keys(data));
});