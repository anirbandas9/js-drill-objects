const data = require('../data.cjs');
const values = require('../values.cjs');

test('Testing Values function', () => {
    expect(values(data)).toStrictEqual(Object.values(data));
});