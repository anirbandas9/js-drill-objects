const data = require("../data.cjs");
const mapObject = require("../mapObject.cjs");

function cb(value) {
  if (typeof value === "string") {
    return value + "2";
  } else {
    return value + 2;
  }
}

const result = {name: "Bruce Wayne2", age: 38, location: "Gotham2"};

test("Testing mapObject function", () => {
  expect(mapObject(data, cb)).toStrictEqual(result);
});