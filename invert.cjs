const elements = require('./data.cjs');

function invert(elements) {
    const result = {};

    if (typeof(elements) !== 'object' || invert.arguments.length !== 1) {
        return {};
    }

    for (let key in elements) {

        if (typeof(elements[key]) === 'function' || typeof(elements[key]) === 'object') {
            continue;
        }
        else {
            result[elements[key]] = key;
        }
    }
    return result;
}

// let a = invert(elements);

// console.log(a);

module.exports = invert;